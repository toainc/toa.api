/*jslint node: true, indent: 2 */
'use strict';
var restify, bunyan, routes, log, server, passport, Strategy, http;

http = require('http');
Strategy = require('passport-http').BasicStrategy;
passport = require('passport');
restify = require('restify');
bunyan = require('bunyan');
routes = require('./routes/');

log = bunyan.createLogger({
  name: 'toa-api',
  level: process.env.LOG_LEVEL || 'info',
  stream: process.stdout,
  serializers: bunyan.stdSerializers
});

server = restify.createServer({
  name: 'toa-api',
  log: log,
  formatters: {
    'application/json': function(req, res, body) {
      res.setHeader('Cache-Control', 'must-revalidate');

      // Does the client *explicitly* accepts application/json?
      var sendPlainText = (req.header('Accept').split(/, */).indexOf('application/json') === -1);

      // Send as plain text
      if (sendPlainText) {
        res.setHeader('Content-Type', 'text/plain; charset=utf-8');
      }

      // Send as JSON
      if (!sendPlainText) {
        res.setHeader('Content-Type', 'application/json; charset=utf-8');
      }
      return JSON.stringify(body);
    }
  }
});

server.use(restify.bodyParser({
  mapParams: false
}));
server.use(restify.queryParser());
server.use(restify.gzipResponse());
server.pre(restify.pre.sanitizePath());

/*jslint unparam:true*/
// Default error handler. Personalize according to your needs.
server.on('uncaughtException', function(req, res, err) {
  console.log('Error!');
  console.log(err);
  res.send(500, {
    success: false
  });
});
/*jslint unparam:false*/

server.on('after', restify.auditLogger({
  log: log
}));
passport.use(new Strategy(
  function(user, password, done) {
    if (!user || !password)
      return done(null, false, {message: "no user or password provided"});
    if (user == "neo4j" && password == "Deportistas10")
      return done(null, {
        user: "neo4j",
        password: "Deportistas10"
      });

    return done(null, false, {message: "auth failed"});
  }
));
routes(server, passport, http);

console.log('Server started.');
server.listen(8888, function() {
  log.info('%s listening at %s', server.name, server.url);
});
